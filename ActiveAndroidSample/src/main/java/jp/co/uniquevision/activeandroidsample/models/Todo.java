package jp.co.uniquevision.activeandroidsample.models;

import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.util.Date;

/**
 * Created by tajima-junpei on 13/12/13.
 */
@Table(name = "todos", id = BaseColumns._ID)
public class Todo extends Model {

  @Column(name = "title") public String title;
  @Column(name = "done") public boolean done = false;
  @Column(name = "created_at") public Date createdAt;
  @Column(name = "updated_at") public Date updatedAt;

  public Todo() {
    super();

    if (updatedAt == null) {
      // new record
      createdAt = updatedAt = new Date();
    } else {
      updatedAt = new Date();
    }
  }
}
